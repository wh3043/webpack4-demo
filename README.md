# webpack4-demo

#### 介绍
webpack4 使用    来源[webpack 4.2 半小时速成](https://www.bilibili.com/video/BV1Cb411e7AR)

webpack.config.js
```javascript
const path = require("path")
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.join[__dirname, 'dist']
  },
  devServer: {
    contentBase: './dist',
    hot: true,
    port: 3000,
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.(htm|html)/,
        use: ['html-withimg-loader'],
      },
      
      {
        test: /\.(png|jpg|svg|gif)/,
        use: [
          {
            loader: 'file-loader',
            options: {
                esModule: false,
                outputPath: 'assets'
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'Webpack Hello',
      filename: 'index.html',
      template: './src/index.html',
    })
  ],

}
```

package.json

```json
{
  "name": "webpackstudy",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "start": "webpack-dev-server",
    "build": "webpack"
  },
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "file-loader": "^6.2.0",
    "html-webpack-plugin": "^4.5.2",
    "html-withimg-loader": "^0.1.16",
    "raw-loader": "^4.0.2",
    "webpack": "^4.46.0",
    "webpack-cli": "^3.3.12",
    "webpack-dev-server": "^3.11.2"
  },
  "dependencies": {
    "css-loader": "^5.0.2",
    "style-loader": "^2.0.0"
  }
}

```

npm install
npm start
