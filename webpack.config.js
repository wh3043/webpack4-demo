const path = require("path")
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.join[__dirname, 'dist']
  },
  devServer: {
    contentBase: './dist',
    hot: true,
    port: 3000,
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.(htm|html)/,
        use: ['html-withimg-loader'],
      },
      
      {
        test: /\.(png|jpg|svg|gif)/,
        use: [
          {
            loader: 'file-loader',
            options: {
                esModule: false,
                outputPath: 'assets'
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'Webpack Hello',
      filename: 'index.html',
      template: './src/index.html',
    })
  ],

}